import re
import scrapy
from scrapy import Request

class JcspiderSpider(scrapy.Spider):
    name = 'JCSpider'
    start_urls = ['https://www.jcsalesweb.com']

    # Main Function
    def parse(self, response):
        # Extract category URLs
        category_links = response.xpath('//li[@class="nav-category-item"]/a/@href').extract()
        # Loop Categories
        yield from response.follow_all(category_links, self.parse_main)

    def parse_main(self, response):
        # Loop items
        for i in response.xpath('//div[contains(@class,"product-list-item")]'):
            # Data dictionary
            item = {
                'VENDORID': '1055',
                'VENDOR': 'JC SALES',
                'ITEMNO': i.xpath('.//div[contains(text(),"Item No.")]/text()').get().replace('Item No.', '').strip(),
                'UPC': '',
                'CATEGORY': '',
                'DESCRIPTION': i.xpath('.//div[contains(@class,"product-name")]/a[1]/text()').get(),
                'IMAGE_URL': i.xpath('.//div[contains(@class,"product-image")]//img[1]/@src').get(),
                'COST': i.xpath('.//span[@class="pack-price"]/text()').get().replace('/', '').strip(),
                'CASEPACK': i.xpath('.//div[contains(@class,"product-umdescription")]/text()').re(r'\d+')[0],
                'PK_SIZE': i.xpath('.//div[contains(@class,"product-umdescription")]/text()').re(r'\d+')[1],
                'DESCRIPTION2': '',
                'PAGE_TITLE': response.css('title::text').get(),
                'PAGE_URL': response.request.url

            }
            yield Request(response.urljoin(i.xpath('.//div[contains(@class,"product-name")]/a[1]/@href').get()),
                          self.parse_details, meta={'item': item})

        # Pagination
        next_page = response.xpath('//a[text()=">"]/@href').get()
        if next_page is not None:
            next_page = response.urljoin(next_page)
            yield scrapy.Request(next_page, callback=self.parse_main)

    def parse_details(self, response):
        # Item details
        item = response.meta['item']
        item['CATEGORY'] = response.xpath('//span[@class = "breadcrumb"][last()]/a/text()').get()
        barcode_text = " ".join(response.xpath('//div[contains(text(), "Barcode:")]/text()').extract())
        upc = re.search(r'\d+', barcode_text)
        item['UPC'] = upc.group(0) if upc is not None else ''
        yield item
